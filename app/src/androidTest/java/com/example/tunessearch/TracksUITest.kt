package com.example.tunessearch

import android.app.Activity
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import androidx.test.runner.lifecycle.Stage
import com.example.tunessearch.features.tracks.TrackListViewModel
import com.example.tunessearch.features.tracks.TrackType
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.*

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule

@RunWith(AndroidJUnit4::class)
class TracksUITest {
    @get:Rule
    var activityRule = ActivityScenarioRule(MainActivity::class.java)

    private val expectedTitle = "Avatar (2009)"
    private val expectedType = "Feature movie"
    private val searchTerm = "avatar"
    private val selectedTrackType = TrackType("Movie", "movie")

    @Test
    fun should_FetchTracks_On_SearchButtonClick() {
        checkIfSearchBarIsDisplayed()

        onView(withId(R.id.searchTerm)).perform(
            replaceText(searchTerm),
            closeSoftKeyboard()
        )
        onView(withId(R.id.typesDropdown)).perform(click())

        onData(
            allOf(
                `is`(instanceOf(TrackType::class.java)),
                `is`(selectedTrackType)
            )
        ).perform(click())

        onView(withId(R.id.searchButton)).perform(click())
        Thread.sleep(1500)

        onView(withText("Tracks retrieved from endpoint.")).inRoot(
            RootMatchers.withDecorView(
                not(`is`(EspressoHelper.getCurrentActivity()!!.window.decorView))
            )
        ).check(
            matches(isDisplayed())
        )

        onView(withChild(withId(R.id.itemTrack))).check(matches(isDisplayed()))
        onView(withId(R.id.tracksList)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )
        Thread.sleep(1500)

    }

    @Test
    fun should_ReturnError_When_SearchTermIsEmpty() {
        checkIfSearchBarIsDisplayed()
        onView(withId(R.id.searchTerm)).perform(
            replaceText(""),
            closeSoftKeyboard()
        )

        onView(withId(R.id.searchButton)).perform(click())

        Thread.sleep(1500)
        onView(withId(R.id.searchTerm)).check(matches(hasErrorText("Search term should not be empty.")))
    }

    @Test
    fun should_OpenDetailsScreen_On_TrackClick() {
        onView(withId(R.id.searchTerm)).perform(
            replaceText(searchTerm),
            closeSoftKeyboard()
        )
        onView(withId(R.id.typesDropdown)).perform(click())

        onData(
            allOf(
                `is`(instanceOf(TrackType::class.java)),
                `is`(selectedTrackType)
            )
        ).perform(click())

        onView(withId(R.id.searchButton)).perform(click())
        Thread.sleep(1500)

        onView(withChild(withId(R.id.itemTrack))).check(matches(isDisplayed()))

        onView(withId(R.id.tracksList)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )

        Thread.sleep(1500)

        onView(withId(R.id.trackName)).check(matches(isDisplayed()))
        onView(withId(R.id.trackName)).check(matches(withText(expectedTitle)))
        onView(withId(R.id.artwork)).check(matches(isDisplayed()))
        onView(withId(R.id.artistName)).check(matches(isDisplayed()))
        onView(withId(R.id.trackType)).check(matches(isDisplayed()))
        onView(withId(R.id.trackType)).check(matches(withText(expectedType)))
        onView(withId(R.id.genre)).check(matches(isDisplayed()))
        onView(withId(R.id.price)).check(matches(isDisplayed()))
    }

    @Test
    fun should_DisplayRetrievedFromDB_On_BackFromDetailsScreen() {
        onView(withId(R.id.searchTerm)).perform(
            replaceText(searchTerm),
            closeSoftKeyboard()
        )
        onView(withId(R.id.typesDropdown)).perform(click())

        onData(
            allOf(
                `is`(instanceOf(TrackType::class.java)),
                `is`(selectedTrackType)
            )
        ).perform(click())

        onView(withId(R.id.searchButton)).perform(click())
        Thread.sleep(1500)

        onView(withChild(withId(R.id.itemTrack))).check(matches(isDisplayed()))

        onView(withId(R.id.tracksList)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )

        Thread.sleep(1500)

        onView(isRoot()).perform(pressBack())

        Thread.sleep(500)

        onView(withText("Tracks retrieved from database.")).inRoot(
            withDecorView(
                not(`is`(EspressoHelper.getCurrentActivity()!!.window.decorView))
            )
        ).check(
            matches(isDisplayed())
        )
    }

    private fun checkIfSearchBarIsDisplayed() {
        onView(withId(R.id.searchBar)).check(matches(isDisplayed()))
        onView(withId(R.id.searchTerm)).check(matches(isDisplayed()))
        onView(withId(R.id.typesDropdown)).check(matches(isDisplayed()))
        onView(withId(R.id.searchButton)).check(matches(isDisplayed()))
    }

    object EspressoHelper {
        fun getCurrentActivity(): Activity? {
            var currentActivity: Activity? = null
            InstrumentationRegistry.getInstrumentation()
                .runOnMainSync {
                    run {
                        currentActivity =
                            ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(
                                Stage.RESUMED
                            ).elementAtOrNull(0)
                    }
                }
            return currentActivity
        }
    }
}