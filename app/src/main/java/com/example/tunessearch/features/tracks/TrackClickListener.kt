package com.example.tunessearch.features.tracks

import android.view.View

interface TrackClickListener {
    fun onTrackClicked(view: View)
}