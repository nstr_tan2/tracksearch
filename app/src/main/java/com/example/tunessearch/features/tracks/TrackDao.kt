package com.example.tunessearch.features.tracks

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface TrackDao {
    @Insert
    suspend fun insertAll(vararg tracks: TrackDetails): List<Long>

    @Query("SELECT * FROM trackdetails")
    suspend fun getAllTracks(): List<TrackDetails>

    @Query("SELECT * FROM trackdetails WHERE id = :trackId")
    suspend fun getTrack(trackId: Int): TrackDetails

    @Query("DELETE FROM trackdetails")
    suspend fun deleteAllTracks()
}