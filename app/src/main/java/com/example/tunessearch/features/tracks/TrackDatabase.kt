package com.example.tunessearch.features.tracks

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

/*
* Room Database
* Handles local database access easily without having to manage boilerplate code
*/
@Database(entities = arrayOf(TrackDetails::class), version = 1)
abstract class TrackDatabase : RoomDatabase() {
    abstract fun trackDao(): TrackDao

    companion object {
        @Volatile
        private var instance: TrackDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            TrackDatabase::class.java,
            "trackdatabase"
        )
            .allowMainThreadQueries()
            .build()
    }
}