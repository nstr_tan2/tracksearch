package com.example.tunessearch.features.tracks

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("resultCount")
    val resultCount: Int,
    @SerializedName("results")
    val results: List<TrackDetails>
)

@Entity
data class TrackDetails(

    @ColumnInfo(name = "id")
    @SerializedName("trackId", alternate = ["collectionId"])
    @PrimaryKey val id: Long?,

    @ColumnInfo(name = "name")
    @SerializedName(value = "trackName", alternate = ["collectionName"])
    val trackName: String?,

    @ColumnInfo(name = "kind")
    @SerializedName("kind", alternate = ["wrapperType"])
    var trackType: String?,

    @ColumnInfo(name = "currency")
    @SerializedName("currency")
    var currency: String = "USD",

    @ColumnInfo(name = "price")
    @SerializedName("trackPrice", alternate = ["collectionPrice"])
    var price: Float = 0f,

    @ColumnInfo(name = "image_url")
    @SerializedName("artworkUrl100")
    val imageUrl: String?,

    @ColumnInfo(name = "genre")
    @SerializedName("primaryGenreName")
    val genre: String?,

    @ColumnInfo(name = "artist_name")
    @SerializedName("artistName")
    val artistName: String?
)

data class TrackType(val text: String, val value: String) {
    override fun toString(): String {
        return text
    }
}