package com.example.tunessearch.features.tracks

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.tunessearch.R
import com.example.tunessearch.databinding.FragmentTrackDetailsBinding
import com.example.tunessearch.extensions.getProcessDrawable
import com.example.tunessearch.extensions.loadImage
import kotlinx.android.synthetic.main.fragment_track_details.*
import kotlinx.android.synthetic.main.fragment_track_details.view.*
import kotlinx.android.synthetic.main.item_track.*
import kotlinx.android.synthetic.main.item_track.artistName
import kotlinx.android.synthetic.main.item_track.view.*

/**
 * A simple [Fragment] subclass.
 * Use the [TrackDetailsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TrackDetailsFragment : Fragment() {
    private var trackId: Int = 0
    private lateinit var viewModel: TrackDetailsViewModel
    private lateinit var dataBinding: FragmentTrackDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        dataBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_track_details, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            trackId = TrackDetailsFragmentArgs.fromBundle(it).trackId
        }
        viewModel = ViewModelProviders.of(this).get(TrackDetailsViewModel::class.java)
        viewModel.load(trackId)
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.trackLiveData.observe(this, Observer { track ->
            track?.let {
                dataBinding.track = track
            }
        })
    }
}