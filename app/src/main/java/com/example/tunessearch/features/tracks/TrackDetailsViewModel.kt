package com.example.tunessearch.features.tracks

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.tunessearch.core.BaseViewModel
import kotlinx.coroutines.launch

class TrackDetailsViewModel(application: Application) : BaseViewModel(application) {
    val trackLiveData = MutableLiveData<TrackDetails>()

    fun load(trackId: Int) {
        launch {
            val dao = TrackDatabase(getApplication()).trackDao()
            val track = dao.getTrack(trackId)
            track.trackType?.let { type ->
                track.trackType = type.replace("-"," ", false).capitalize()
            }

            trackLiveData.value = track
        }
    }
}