package com.example.tunessearch.features.tracks

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.tunessearch.R
import com.example.tunessearch.databinding.ItemTrackBinding
import com.example.tunessearch.extensions.getProcessDrawable
import com.example.tunessearch.extensions.loadImage
import kotlinx.android.synthetic.main.item_track.view.*

class TrackListAdapter(val trackList: ArrayList<TrackDetails>) :
    RecyclerView.Adapter<TrackListAdapter.TrackViewHolder>(), TrackClickListener {

    class TrackViewHolder(var view: ItemTrackBinding) : RecyclerView.ViewHolder(view.root)

    fun updateSongList(newTrackList: List<TrackDetails>) {
        trackList.clear()
        trackList.addAll(newTrackList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = DataBindingUtil.inflate<ItemTrackBinding>(inflater, R.layout.item_track, parent, false)
        return TrackViewHolder(view)
    }

    override fun getItemCount(): Int = trackList.size

    override fun onBindViewHolder(holder: TrackViewHolder, position: Int) {
        holder.view.track = trackList[position]
        holder.view.listener = this
    }

    override fun onTrackClicked(v: View) {
        val action = TrackListFragmentDirections.actionTrackListFragmentToTrackDetailsFragment()
        action.trackId = v.trackId.text.toString().toInt()
        Navigation.findNavController(v).navigate(action)
    }
}