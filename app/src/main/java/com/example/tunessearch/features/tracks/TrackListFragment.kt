package com.example.tunessearch.features.tracks

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tunessearch.R
import kotlinx.android.synthetic.main.fragment_track_list.*

class TrackListFragment : Fragment(), AdapterView.OnItemSelectedListener {

    private lateinit var viewModel: TrackListViewModel
    private val trackListAdapter = TrackListAdapter(arrayListOf())
    private var trackTypeList = ArrayList<TrackType>()
    private var currentlySelectedTrackType = "music";

    init {
        trackTypeList.add(TrackType("Music", "music"))
        trackTypeList.add(TrackType("Movie", "movie"))
        trackTypeList.add(TrackType("TV Show", "tvShow"))
        trackTypeList.add(TrackType("Podcast", "podcast"))
        trackTypeList.add(TrackType("Music Video", "musicVideo"))
        trackTypeList.add(TrackType("Audio Book", "audiobook"))
        trackTypeList.add(TrackType("Short Film", "shortFilm"))
        trackTypeList.add(TrackType("Software", "software"))
        trackTypeList.add(TrackType("All", "all"))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_track_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Set Listeners for View Elements interactions
        searchButton.setOnClickListener {
            onSearchButtonClicked()
        }

        searchTerm.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                //Perform Code
                onSearchButtonClicked()
                return@OnKeyListener true
            }
            false
        })

        var spinnerAdapter = ArrayAdapter(typesDropdown.context, android.R.layout.simple_spinner_item, trackTypeList)
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item)
        with(typesDropdown) {
            adapter = spinnerAdapter
            setSelection(0)
            onItemSelectedListener = this@TrackListFragment
        }

        viewModel = ViewModelProviders.of(this).get(TrackListViewModel::class.java)
        searchTerm.setText(viewModel.getSearchTerm())
        viewModel.refresh()

        tracksList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = trackListAdapter
        }

        refreshLayout.setOnRefreshListener {
            tracksList.visibility = View.GONE
            listError.visibility = View.GONE
            loadingView.visibility = View.VISIBLE
            viewModel.refreshBypassCache()
            refreshLayout.isRefreshing = false
        }
        observeViewModel()
    }

    fun onSearchButtonClicked() {
        val term = searchTerm.text.toString()
        hideKeyboard(searchTerm)
        searchButton.isEnabled = false
        if(term.isNotEmpty()) {
            searchTerm.error = null
            viewModel.searchTracks(term, currentlySelectedTrackType)
        } else {
            searchTerm.error = "Search term should not be empty."
        }

    }

    fun observeViewModel() {
        viewModel.tracks.observe(this, Observer { songs ->
            songs?.let {
                tracksList.visibility = View.VISIBLE
                trackListAdapter.updateSongList(songs)
            }
            searchButton.isEnabled = true
        })

        viewModel.hasLoadError.observe(this, Observer { isError ->
            isError?.let {
                listError.visibility = if (it) View.VISIBLE else View.GONE
                searchButton.isEnabled = true
            }
        })

        viewModel.isLoading.observe(this, Observer { isLoading ->
            isLoading?.let {
                loadingView.visibility = if (it) View.VISIBLE else View.GONE
                if (it) {
                    listError.visibility = View.GONE
                    tracksList.visibility = View.GONE
                }
                searchButton.isEnabled = true
            }
        })
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, viewId: Long) {
        currentlySelectedTrackType = trackTypeList[position].value
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        currentlySelectedTrackType = "music"
    }
    private fun hideKeyboard(view: View) {
        if(view.hasFocus()) {
            val manager :InputMethodManager =  context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            manager?.let {
                manager.hideSoftInputFromWindow(view.windowToken,0)
            }
        }
    }
}