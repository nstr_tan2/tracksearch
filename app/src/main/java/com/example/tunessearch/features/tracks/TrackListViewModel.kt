package com.example.tunessearch.features.tracks

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.tunessearch.core.BaseViewModel
import com.example.tunessearch.util.SharedPreferencesHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch

class TrackListViewModel(application: Application) : BaseViewModel(application) {

    private var prefHelper = SharedPreferencesHelper(getApplication())
    private var refreshTime = 5 * 60 * 1000 * 1000 * 1000L

    private val tracksService = TraksApiService()
    private val disposable = CompositeDisposable()

    private var searchTerm = ""
    private var searchType = "music"

    val tracks = MutableLiveData<List<TrackDetails>>()
    val hasLoadError = MutableLiveData<Boolean>()
    val isLoading = MutableLiveData<Boolean>()

    fun refresh() {
        val updateTime = prefHelper.getUpdateTime()
        if (updateTime != null && updateTime != 0L && System.nanoTime() - updateTime < refreshTime) {
            fetchFromDatabase()
        } else {
            fetchFromRemote()
        }
    }

    fun getSearchTerm(): String {
        searchTerm = prefHelper.getSearchTerm() ?: ""
        return searchTerm
    }

    fun refreshBypassCache() {
        fetchFromRemote()
    }

    fun searchTracks(term: String, type: String) {
        searchTerm = term
        searchType = type
        prefHelper.saveLastSearch(term)
        fetchFromRemote()

    }

    private fun fetchFromRemote() {
        if (searchTerm.isNotEmpty()) {
            isLoading.value = true
            val term = searchTerm
            val media = searchType
            disposable.add(
                tracksService.getTracks(term, media)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : DisposableSingleObserver<Response>() {

                        override fun onSuccess(response: Response) {
                            storeTracksLocally(response.results)
                            Toast.makeText(
                                getApplication(),
                                "Tracks retrieved from endpoint.",
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }

                        override fun onError(e: Throwable) {
                            hasLoadError.value = true
                            isLoading.value = false
                        }
                    })
            )
        } else {
            hasLoadError.value = false
            isLoading.value = false
        }

    }


    private fun tracksRetrieved(trackList: List<TrackDetails>) {
        tracks.value = trackList.sortedBy { it.id }
        hasLoadError.value = false
        isLoading.value = false
    }

    private fun storeTracksLocally(trackList: List<TrackDetails>) {
        launch {
            val dao = TrackDatabase(getApplication()).trackDao()
            dao.deleteAllTracks()
            dao.insertAll(*trackList.toTypedArray())
            tracksRetrieved(trackList)
        }
        prefHelper.saveUpdateTime(System.nanoTime())
    }

    private fun fetchFromDatabase() {
        isLoading.value = true
        launch {
            val tracks = TrackDatabase(getApplication()).trackDao().getAllTracks()
            if(tracks != null) {
                tracksRetrieved(tracks)
                Toast.makeText(getApplication(), "Tracks retrieved from database.", Toast.LENGTH_SHORT)
                    .show()
            }

        }
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}