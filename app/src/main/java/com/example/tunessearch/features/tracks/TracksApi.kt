package com.example.tunessearch.features.tracks

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface TracksApi {
    @GET("search")
    fun getTracks(@Query("term") term: String = "a", @Query("media") media: String = "music"): Single<Response>
}