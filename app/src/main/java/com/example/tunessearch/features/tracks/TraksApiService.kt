package com.example.tunessearch.features.tracks

import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class TraksApiService {

    private val BASE_URL = "https://itunes.apple.com/"

    private val api = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(TracksApi::class.java)

    fun getTracks(terms: String, media: String): Single<Response> {
        return api.getTracks(term = terms, media = media)

    }
}