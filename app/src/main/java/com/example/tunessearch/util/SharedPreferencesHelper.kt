package com.example.tunessearch.util

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.preference.PreferenceManager

class SharedPreferencesHelper {
    companion object {
        private const val PREF_TIME = "Pref time"
        private const val LAST_SEARCH = "Last search term"
        private var prefs: SharedPreferences? = null

        @Volatile
        private var instance: SharedPreferencesHelper? = null
        private val LOCK = Any()

        operator fun invoke(context: Context): SharedPreferencesHelper =
            instance ?: synchronized(LOCK) {
                instance ?: buildHelper(context).also {
                    instance = it
                }
            }

        private fun buildHelper(context: Context): SharedPreferencesHelper {
            prefs = PreferenceManager.getDefaultSharedPreferences(context)
            return SharedPreferencesHelper()
        }
    }

    fun saveUpdateTime(time: Long) {
        prefs?.edit(commit = true) {
            putLong(PREF_TIME, time)
        }
    }

    fun saveLastSearch(term: String) {
        prefs?.edit(commit = true) {
            putString(LAST_SEARCH, term)
        }
    }

    fun getUpdateTime() = prefs?.getLong(PREF_TIME, 0)
    fun getSearchTerm() = prefs?.getString(LAST_SEARCH, "")
}