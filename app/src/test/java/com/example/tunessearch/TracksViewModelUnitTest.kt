package com.example.tunessearch

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.tunessearch.features.tracks.TrackDetails
import com.example.tunessearch.features.tracks.TrackListViewModel
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Test
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runner.RunWith
import org.junit.runners.model.Statement

@RunWith(AndroidJUnit4::class)
class TracksViewModelUnitTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val rule = TrampolineSchedulerRule()

    private lateinit var viewModel: TrackListViewModel

    @Before
    fun setup() {
        viewModel = TrackListViewModel(ApplicationProvider.getApplicationContext())
    }

    @Test
    fun `should fetch tracks when search track parameters are fulfilled `() {
        viewModel.searchTracks("avatar", "movie")
        val tracks = viewModel.tracks.value!!
        assertTrue(viewModel.hasLoadError.value == false)
        assertTrue(tracks.isNotEmpty())
        assertTrue(tracks[0]::class == TrackDetails::class)
        assertTrue(tracks[0].trackName == "Avatar (2009)")
    }

    @Test
    fun `should have error state true when search track parameters are invalid `() {
        viewModel.searchTracks("avatar", "movi")
        assertTrue(viewModel.hasLoadError.value == true)
    }
}

class TrampolineSchedulerRule : TestRule {
    private val scheduler by lazy { Schedulers.trampoline() }
    override fun apply(base: Statement?, description: Description?): Statement =
        object : Statement() {
            override fun evaluate() {
                try {
                    RxJavaPlugins.setComputationSchedulerHandler { scheduler }
                    RxJavaPlugins.setIoSchedulerHandler { scheduler }
                    RxJavaPlugins.setNewThreadSchedulerHandler { scheduler }
                    RxJavaPlugins.setSingleSchedulerHandler { scheduler }
                    RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler }
                    base?.evaluate()
                } finally {
                    RxJavaPlugins.reset()
                    RxAndroidPlugins.reset()
                }
            }
        }
}